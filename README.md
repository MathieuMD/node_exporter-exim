These Bash scripts expose [Exim](http://www.exim.org/) queue status (`exiqsumm`)
and statistics (`eximstats`) to [Prometheus](https://prometheus.io/)
through [node_exporter](https://github.com/prometheus/node_exporter)'s
`textfile` collector.

They could be launched by `root` from it's crontab:

```bash
sudo crontab -l
#* * * * *   /bin/bash /opt/node_exporter-mailq.sh
#*/5 * * * * /bin/bash /opt/node_exporter-eximstats.sh
```

They create text files under `/var/lib/prometheus/node-exporter`, where
Debian's package of `node_exporter` is configured by default to look for
`*.prom` text files in the [right
format](https://prometheus.io/docs/instrumenting/exposition_formats/).

License: GPLv3
